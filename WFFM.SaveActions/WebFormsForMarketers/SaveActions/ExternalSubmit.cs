﻿using System;
using System.Net;
using System.Text;
using Sitecore.Diagnostics;
using Sitecore.Form.Core.Controls.Data;
using Sitecore.Form.Submit;
using System.IO;
using System.Web;

namespace WFFM.SaveActions.WebFormsForMarketers.SaveActions
{
    public class ExternalSubmit : ISaveAction
    {
        public string PostUrl { get; set; }
        public string CharsetEncoding { get; set; }
        public string SubmissionFields { get; set; }
        public string AdditionalData { get; set; }

        public void Execute(Sitecore.Data.ID formid, Sitecore.Form.Core.Client.Data.Submit.AdaptedResultList fields,
            params object[] data)
        {
            Assert.IsNotNull(PostUrl, "PostUrl Is Null!");

            SubmitForm(fields);
        }

        private void SubmitForm(Sitecore.Form.Core.Client.Data.Submit.AdaptedResultList fields)
        {
            StringBuilder postBuilder = new StringBuilder();

            // Ensure that the Additional Post Data is in correct format.
            if (!string.IsNullOrEmpty(AdditionalData))
            {
                string[] additionalDataValues = AdditionalData.Split('&');

                foreach (string postField in additionalDataValues)
                {
                    string[] fieldAsKeyValue = postField.Split('=');

                    if (fieldAsKeyValue.Length == 2)
                    {
                        postBuilder.AppendFormat("{0}={1}&", HttpUtility.UrlEncode(fieldAsKeyValue[0]),
                            HttpUtility.UrlEncode(fieldAsKeyValue[1]));
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(SubmissionFields))
            {
                string[] fieldsToSubmit = SubmissionFields.Split(new[] {"||"}, StringSplitOptions.RemoveEmptyEntries);

                foreach (string fieldToSubmit in fieldsToSubmit)
                {
                    AdaptedControlResult formField = fields.GetEntryByID(fieldToSubmit);

                    if (formField != null)
                    {
                        postBuilder.AppendFormat("{0}={1}&", HttpUtility.UrlEncode(formField.FieldName),
                            HttpUtility.UrlEncode(formField.Value));
                    }
                }
            }

            postBuilder.Length--;

            try
            {
                HttpWebRequest httpRequest =
                    (HttpWebRequest) WebRequest.Create(PostUrl);
                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";

                Encoding encoding = Encoding.Default;

                if (!string.IsNullOrEmpty(CharsetEncoding))
                {
                    encoding = Encoding.GetEncoding(CharsetEncoding);
                }

                byte[] formData = encoding.GetBytes(postBuilder.ToString());

                httpRequest.ContentLength = formData.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(formData, 0, formData.Length);
                requestStream.Close();

                using (HttpWebResponse response = (HttpWebResponse) httpRequest.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        Log.Warn(string.Format("Post To {0} returned: {1}", PostUrl, response.StatusCode), this);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("External Submit Threw and Exception", ex, this);
            }

        }
    }
}