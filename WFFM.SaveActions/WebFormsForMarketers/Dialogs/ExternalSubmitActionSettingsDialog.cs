﻿using System.Collections.Specialized;
using System.Linq;
using Sitecore;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Form.Core.Utility;
using Sitecore.Forms.Core.Data;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using System;
using System.Web;
using Sitecore.Web.UI.Sheer;

namespace WFFM.SaveActions.WebFormsForMarketers.Dialogs
{
    public class ExternalSubmitActionSettingsDialog : DialogForm
    {
        private readonly Database _sitecoreDatabase = Factory.GetDatabase(Sitecore.Web.WebUtil.GetQueryString("db"));
        private readonly string _formId = Sitecore.Web.WebUtil.GetQueryString("id");

        protected Edit PostUrl;
        protected Edit CharsetEncoding;
        protected Checklist SubmissionFields;
        protected Edit AdditionalData;

        private NameValueCollection _parameters;

        public NameValueCollection Parameters
        {
            get
            {
                if (_parameters == null)
                {
                    string parametersKey = Sitecore.Web.WebUtil.GetQueryString("params");
                    _parameters =
                        ParametersUtil.XmlToNameValueCollection(HttpContext.Current.Session[parametersKey].ToString());
                }

                return _parameters;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Context.ClientPage.IsEvent)
            {
                RestoreForm();
            }
        }

        protected override void OnOK(object sender, EventArgs args)
        {
            SheerResponse.SetDialogValue(
                ParametersUtil.NameValueCollectionToXml(new NameValueCollection
                {
                    {"PostUrl", PostUrl.Value},
                    {"CharsetEncoding", CharsetEncoding.Value},
                    {
                        "SubmissionFields",
                        string.Join("||",
                            SubmissionFields.Items.Where(x => x.Checked)
                                .Select(x => x.Value))
                    },
                    {"AdditionalData", AdditionalData.Value}
                }));

            base.OnOK(sender, args);
        }

        private void RestoreForm()
        {
            if (Parameters != null)
            {
                PostUrl.Value = Parameters["PostUrl"] ?? string.Empty;
                CharsetEncoding.Value = Parameters["CharsetEncoding"] ?? string.Empty;
                AdditionalData.Value = Parameters["AdditionalData"] ?? string.Empty;

                string[] selectedFields = {};

                if (!string.IsNullOrEmpty(Parameters["SubmissionFields"]))
                {
                    selectedFields = Parameters["SubmissionFields"].Split(new[] {"||"},
                        StringSplitOptions.RemoveEmptyEntries);
                }

                if (_sitecoreDatabase != null && !string.IsNullOrEmpty(_formId))
                {
                    FormItem currentForm = _sitecoreDatabase.GetItem(_formId);

                    if (currentForm != null && currentForm.FieldItems.Any())
                    {
                        foreach (FieldItem field in currentForm.FieldItems)
                        {
                            SubmissionFields.Controls.Add(
                                new ChecklistItem
                                {
                                    Header = field.Name,
                                    Checked = selectedFields.Contains(field.ID.ToString()),
                                    Value = field.ID.ToString()
                                });
                        }
                    }
                }
            }
        }
    }
}